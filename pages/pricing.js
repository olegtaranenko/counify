import React, { Component } from 'react'
import ButtonLink from './../src/components/ButtonLink'
import Meta from './../src/partials/Meta'

class Pricing extends Component {
  render () {
    return (
      <div>
        <Meta
          title='Pricing | Unify'
          desc='Give your company the upper hand with the ability to organize and strategically implement feature requests from current and potential customers.'
          canonical='https://www.getunify.co/pricing'
        />
        <h1 className='text-center'><span className='underline'>Pricin</span>g</h1>
        <div id='pricing-table'>
          <div className='price'>
            <div className='pricing-header'>
              <img src='/images/mail.png' alt='Mail' />
              <span className='title'>Alpha Users Get Unify Free!</span>
            </div>
            <div className='pricing-info'>
              <span>We keep things simple. We have one plan with everything you need.</span>
              <span>Only 10 Free Alpha Spots Available!</span>
            </div>
            <ButtonLink type='primary' href='/alpha-register'>Apply for Alpha Status!</ButtonLink>
          </div>
          <div className='price'>
            <div className='pricing-header'>
              <span className='title'>Everything You Need</span>
              <img src='/images/rocket.png' alt='Rocket' />
              <div className='price-amount'>
                <span className='dollar-sign'>$</span>
                <span className='dollar-amount'>50</span>
                <span className='subscription-length'>/month</span>
              </div>
            </div>
            <div className='pricing-info'>
              <span>Unlimited Feature Requests</span>
              <span>Feature Request Organizer</span>
              <span>Customer Portal</span>
              <span>Tags</span>
              <span>Much More!</span>
            </div>
            <ButtonLink type='primary' href='/register'>Try Free!</ButtonLink>
          </div>
          <div className='price'>
            <div className='pricing-header'>
              <span className='title'>Enterprise</span>
              <img src='/images/crown.png' alt='Crown' />
              <span className='title'>Custom</span>
            </div>
            <div className='pricing-info'>
              <span>Whitelabling</span>
              <span>Enterprise Level Security</span>
              <span>Dedicated Account Management</span>
            </div>
            <ButtonLink type='primary' href='/sales'>Contact Sales</ButtonLink>
          </div>
        </div>
        <style jsx>{`
          
        `}</style>
      </div>
    )
  }
}

export default Pricing
