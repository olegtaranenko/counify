import React from 'react'
import { loadStripe } from '@stripe/stripe-js'
import { Elements, ElementsConsumer } from '@stripe/react-stripe-js'
import Meta from './../src/partials/Meta'
import RegisterContainer from './../src/containers/RegisterContainer'
import config from './../src/config'

const Register = () => {
  const stripePromise = loadStripe(config.stripe_key)
  return (
    <div>
      <Meta
        title='Register | Unify'
        desc='Give your company the upper hand with the ability to organize and strategically implement feature requests from current and potential customers.'
        canonical='https://www.getunify.co/register'
      />
      <Elements stripe={stripePromise}>
        <ElementsConsumer>
          {({ stripe, elements }) => (
            <RegisterContainer stripe={stripe} elements={elements} />
          )}
        </ElementsConsumer>
      </Elements>
    </div>
  )
}

export default Register
