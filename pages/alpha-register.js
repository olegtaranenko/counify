import React from 'react'
import Meta from './../src/partials/Meta'
import RegisterContainer from './../src/containers/RegisterContainer'

const AlphaRegister = () => {
  return (
    <div>
      <Meta
        title='Alpha Register | Unify'
        desc='Give your company the upper hand with the ability to organize and strategically implement feature requests from current and potential customers.'
        canonical='https://www.getunify.co/alpha-register'
      />
      <h3 className='text-center'>Apply for Alpha Status to Get Unify Free for Life!</h3>
      <RegisterContainer alpha />
    </div>
  )
}

export default AlphaRegister
