import React, { Component } from 'react'
import axios from 'axios'
import Link from 'next/link'

class Verify extends Component {
  constructor (props) {
    super(props)
    const { router } = this.props
    const queryUrl = router.query.queryURL
    if (queryUrl) {
      // TODO check the url
      axios.post(queryUrl)
    }
  }

  render () {
    return (
      <div className='text-center'>
        <h2>Thanks for verifying your email!</h2>
        <Link href='/login'>
          <a className='btn btn-purple squared' type='button'>Login</a>
        </Link>
      </div>
    )
  }
}

export default Verify
