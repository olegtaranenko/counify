import React, { Component } from 'react'
import { Row, Col } from 'react-bootstrap'
import Button from './../src/components/Button'
import ButtonLink from './../src/components/ButtonLink'
import Meta from './../src/partials/Meta'

class Index extends Component {
  constructor (props) {
    super(props)
    this.listenRef = React.createRef()
    this.organizeRef = React.createRef()
    this.growRef = React.createRef()
  }

  clickScrollTo (ref) {
    window.scrollTo(0, ref.current.offsetTop)
  }

  render () {
    return (
      <div>
        <Meta
          title='Unify'
          desc='Give your company the upper hand with the ability to organize and strategically implement feature requests from current and potential customers.'
          canonical='https://www.getunify.co'
          noFollow
        />
        <h2 className='text-center'>We solve the <span className='underline'>most difficult</span> part of tracking and implementing your companies’ feature requests</h2>
        <br />
        <p className='text-center'>Give your company the upper hand with the ability to organize and strategically implement feature requests from current <span className='underline'>and</span> potential customers.</p>
        <section className='learn-more'>
          <Row>
            <Col md>
              <div className='learn-more-item'>
                <img src='/images/listener.png' alt='Listener' />
                <h3>Listen to Everyone Giving You Feature Ideas</h3>
                <Button type='primary' onClick={this.clickScrollTo.bind(this, this.listenRef)}>Learn More</Button>
              </div>
            </Col>
            <Col md>
              <div className='learn-more-item'>
                <img src='/images/organize.png' alt='Organize' />
                <h3>Organize and Prioritize Feature Requests</h3>
                <Button type='primary' onClick={this.clickScrollTo.bind(this, this.organizeRef)}>Learn More</Button>
              </div>
            </Col>
            <Col md>
              <div className='learn-more-item'>
                <img src='/images/client.png' alt='Client' />
                <h3>Delight Your Customers and Grow</h3>
                <Button type='primary' onClick={this.clickScrollTo.bind(this, this.growRef)}>Learn More</Button>
              </div>
            </Col>
          </Row>
        </section>
        <section ref={this.listenRef}>
          <h2 className='text-center'>Listen To <span className='underline'>Everyone</span> Giving you Feature Ideas</h2>
          <Row>
            <Col md={4}>
              <img src='/images/review.png' alt='Review' className='img-fluid' />
            </Col>
            <Col md={8}>
              <h5 className='text-center'>Current Customers</h5>
              <p>Retention is the first focus in growing your company. For every customer you lose you need to add at least two more! The best way to increase retention is to make your product impossible to walk away from from by implementing your customer’s ideas into your product.</p>
              <p>With Unify, you can easily give your customers the ability to tell you what they need in order to keep your product a "must have" in their world.</p>
              <br />
              <div className='text-center'>
                <ButtonLink href='/pricing-alpha' type='primary'>Get Started</ButtonLink>
              </div>
            </Col>
          </Row>
          <br />
          <Row>
            <Col md={{ span: 4, order: 12 }}>
              <img src='/images/internal-teams.png' alt='Internal Teams' className='img-fluid' />
            </Col>
            <Col md={{ span: 8, order: 1 }}>
              <h5 className='text-center'>Internal Teams</h5>
              <p>Everyone on your team interacts with your product on a different level. Sales Teams, Customer Support Teams, and Development Teams all have unique perspectives on what would help your product be more valuable to your customers.</p>
              <p>With Unify, you are able to give every department in your company certain access to submitting feature requests as well as understanding the roadmap and where your product is headed.</p>
              <br />
              <div className='text-center'>
                <ButtonLink href='/pricing-alpha' type='primary'>Get Started</ButtonLink>
              </div>
            </Col>
          </Row>
          <br />
          <Row>
            <Col md={4}>
              <img src='/images/potential-customer.png' alt='Potential Customer' className='img-fluid' />
            </Col>
            <Col md={8}>
              <h5 className='text-center'>Potential Customers</h5>
              <p>Not listening to your potential customers is like driving a car with a blindfold on. How else will you know how your product should evolve?</p>
              <p>With Unify, your customer facing pre-sales teams such as Account Executives, SDRs, and ISR’s all communicate with hundreds of potential customers every month. Use your customer’s objections to make your product more bulletproof and convert more prospects into paying customers.</p>
              <br />
              <div className='text-center'>
                <ButtonLink href='/pricing-alpha' type='primary'>Get Started</ButtonLink>
              </div>
            </Col>
          </Row>
        </section>
        <section ref={this.organizeRef}>
          <h2 className='underline text-center'>Organize and Prioritize Your Feature Requests</h2>
          <Row>
            <Col md={4}>
              <img src='/images/scatter.png' alt='Scatter' className='img-fluid' />
            </Col>
            <Col md={8} className='text-center'>
              <p>Once you recieve feature requests from customers and internal teams, what do you do with the requests? Track them in endless spreadsheets? Shared slack Channels where they sit with no actionable items or assigned teams?</p>
              <p>With Unify, get the tools that allow you to organize feature requests by tags, priority, ease of implementation, confidence, and impact. Also, easily put them on your roadmap and assign the proper teams to that release.</p>
              <br />
              <div className='text-center'>
                <ButtonLink href='/pricing-alpha' type='primary'>Get Started</ButtonLink>
              </div>
            </Col>
          </Row>
        </section>
        <section ref={this.growRef}>
          <h2 className='underline text-center'>Delight Your Customers and Grow</h2>
          <Row>
            <Col md={8} className='text-center'>
              <p>When you have a repeatable, scalable process, to listen to your customers, you make brand ambassadors and champions that see your product or service is a <span className='underline'>must have</span>.</p>
              <p>Happy customers, internal teams, and potential customers that are listened to cause you to retain customers better, get input from your team in a collaborative environment, and sell to more customers because your product fits the needs of the market.</p>
            </Col>
            <Col md={4} className='d-none d-md-block'>
              <img src='/images/person.png' alt='person' className='person-img' />
            </Col>
          </Row>
        </section>
        <style jsx>{`
          h2 {
            margin-bottom: 30px;
          }
          section {
            margin: 40px 0;
          }
          section:last-child {
            margin-bottom: 0;
          }
          .learn-more {
            text-align: center;
          }
          .learn-more img {
            width: 128px;
          }
          .learn-more .learn-more-item {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: space-between;
            height: 100%;
            padding: 10px 0;
          }
          .person-img {
            max-width: 100%;
          }
        `}</style>
      </div>
    )
  }
}

export default Index
