import React, { Component } from 'react'
import ButtonLink from './../src/components/ButtonLink'
import Meta from './../src/partials/Meta'

class PricingAlpha extends Component {
  render () {
    return (
      <div>
        <Meta
          title='Pricing | Unify'
          desc='Give your company the upper hand with the ability to organize and strategically implement feature requests from current and potential customers.'
          canonical='https://www.getunify.co/pricing-alpha'
        />
        <h1 className='text-center'><span className='underline'>Pricin</span>g</h1>
        <div id='pricing-table'>
          <div className='price'>
            <div className='pricing-header'>
              <img src='/images/mail.png' alt='Mail' />
              <span className='title'>Alpha Users Get Unify Free!</span>
            </div>
            <div className='pricing-info'>
              <span>Apply now to become anAlpha User and get Unify free for lIfe (Some thresholds may apply).</span>
              <span><b>Only 10 Free Alpha Spots Available!</b></span>
            </div>
          </div>
          <div className='price'>
            <div className='pricing-header'>
              <span className='title'>Everything You Need</span>
              <img src='/images/rocket.png' alt='Rocket' />
              <div className='price-amount'>
                <span className='dollar-amount'>Free</span>
              </div>
            </div>
            <div className='pricing-info'>
              <span>Unlimited Feature Requests</span>
              <span>Feature Request Organizer</span>
              <span>Customer Portal</span>
              <span>Tags</span>
              <span>Much More!</span>
            </div>
            <ButtonLink type='primary' href='/alpha-register'>Apply for Alpha Status!</ButtonLink>
          </div>
          <div className='price' id='quote'>
            <span>
                "Unity is strength... when there is teamwork and collaboration, wonderful things can be achieved."
              <br />
              <br />
                - Mattie Stepanek
            </span>
          </div>
        </div>
        <style jsx>{`
            #quote {
                padding: 30px 30px;
                font-size: 34px;
                color: #343434;
            }
        `}</style>
      </div>
    )
  }
}

export default PricingAlpha
