import React from 'react'
import Meta from './../src/partials/Meta'

const RegisterSuccess = () => {
  return (
    <div className='text-center'>
      <Meta
        title='Register Success | Unify'
        desc='Give your company the upper hand with the ability to organize and strategically implement feature requests from current and potential customers.'
        canonical='https://www.getunify.co/register'
        noFollow
      />
      <h2>Thanks for registering!</h2>
      <p>A confirmation email has been sent to you!</p>
    </div>
  )
}

export default RegisterSuccess
