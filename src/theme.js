export default {
  colors: {
    primary: '#b540de',
    gray: '#f8f9fa',
    light_black: '#47474e',
    red: '#A30015'
  }
}
