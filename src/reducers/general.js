import { LOADING } from './../actions/types'

export const initialState = {
  is_loading: false
}

export default function (state = initialState, action) {
  switch (action.type) {
    case LOADING:
      return {
        ...state,
        is_loading: action.payload
      }
    default:
      return state
  }
}
