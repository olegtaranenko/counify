import React from 'react'
import styled from 'styled-components'
import Link from 'next/link'

const Button = styled.a`
  border-radius: 20px;
  padding: 10px;
  color: #fff !important;
  apperance: none !important;
  -webkit-appearance: none !important;
  display: inline-block;
  transition: background-color 0.5s ease;
  :hover {
    background-color: #a840de;
  }
  background-color: ${props => {
    switch (props.type) {
      case 'primary':
        return props.theme.colors.primary
      case 'purple':
        return props.theme.colors.primary
      case 'gray':
        return props.theme.colors.gray
      default:
        return props.theme.colors.primary
    }
  }}
`

const ButtonLink = (props) => {
  return (
    <Link href={props.href}>
      <Button className={props.className} type='button'>{ props.children }</Button>
    </Link>
  )
}

export default ButtonLink
