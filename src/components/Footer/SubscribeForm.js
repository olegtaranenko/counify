
import React, { Component } from 'react'
import { Formik, Field } from 'formik'
import { connect } from 'react-redux'
import * as Yup from 'yup'
import { submitSubscribeForm } from './../../actions/general'

const subscribeSchema = Yup.object().shape({
  email: Yup.string()
    .email('Invalid email')
    .required('Required')
})

class SubscribeForm extends Component {
  onFormSubmit (data, { setSubmitting, resetForm }) {
    setSubmitting(true)
    this.props.submitSubscribeForm(data).then(() => {
      resetForm({})
    })
      .catch(err => {
        console.error(err)
      })
    setSubmitting(false)
  }

  render () {
    return (
      <div>
        <p>Stay In Touch</p>
        <Formik
          validationSchema={subscribeSchema}
          initialValues={{ email: '' }}
          onSubmit={this.onFormSubmit.bind(this)}>
          {({ errors, touched, isSubmitting, handleSubmit }) => (
            <form onSubmit={handleSubmit} className='block-form'>
              <Field name='email' as='input' placeholder='Email address' className={errors.email && touched.email ? 'error' : ''} />
              <button className='btn btn-white' disabled={isSubmitting} type='submit'>Subscribe</button>
            </form>
          )}
        </Formik>
      </div>
    )
  }
}

export default connect(null, {
  submitSubscribeForm
})(SubscribeForm)
