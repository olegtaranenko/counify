
import React, { Component } from 'react'
import { Formik, Field } from 'formik'
import { connect } from 'react-redux'
import * as Yup from 'yup'
import { submitContactForm } from './../../actions/general'

const contactUsSchema = Yup.object().shape({
  email: Yup.string()
    .email('Invalid email')
    .required('Required'),
  message: Yup.string()
    .required('Required')
})

class ContactUsForm extends Component {
  onFormSubmit (data, { setSubmitting, resetForm }) {
    setSubmitting(true)
    this.props.submitContactForm(data).then(() => {
      resetForm({})
    })
      .catch((err) => {
        console.error(err)
      })
    setSubmitting(false)
  }

  render () {
    return (
      <div>
        <p>Get In Touch</p>
        <Formik
          initialValues={{ email: '', message: '' }}
          onSubmit={this.onFormSubmit.bind(this)}
          validationSchema={contactUsSchema}>
          {({ errors, touched, isSubmitting, handleSubmit }) => (
            <form onSubmit={handleSubmit} className='block-form'>
              <Field name='email' as='input' placeholder='Email address' className={errors.email && touched.email ? 'error' : ''} />
              <Field name='message' as='textarea' placeholder='Message' />
              <button className='btn btn-white' disabled={isSubmitting} type='submit'>Contact Us</button>
            </form>
          )}
        </Formik>
      </div>
    )
  }
}

export default connect(null, {
  submitContactForm
})(ContactUsForm)
