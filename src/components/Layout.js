import React, { PureComponent } from 'react'
import { Container } from 'react-bootstrap'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import Header from './Header'
import Footer from './Footer'
import 'bootstrap/dist/css/bootstrap.min.css'
import '../../public/styles/main.scss'
import '../../public/styles/pricing.scss'

export default class Layout extends PureComponent {
  render () {
    return (
      <div className='layout'>
        <header>
          <Header />
        </header>
        <Container className='content'>
          { this.props.children }
        </Container>
        <footer>
          <Footer />
        </footer>
        <ToastContainer autoClose={2000} />
      </div>
    )
  }
}
