import React from 'react'
import Link from 'next/link'
import { Container, Nav, Navbar } from 'react-bootstrap'
import ButtonLink from './ButtonLink'

const Header = () => {
  return (
    <div>
      <Container>
        <Navbar expand='md' className='header-nav'>
          <Link href='/'>
            <Navbar.Brand href='#'>
              <img
                alt='Unify Logo'
                src='/images/logo.png'
                className='d-inline-block align-top logo'
              />
            </Navbar.Brand>
          </Link>
          <Navbar.Toggle aria-controls='header-nav' />
          <div className='top-right d-none d-md-block'>
            <a href='https://app.getunify.co/login'>Login</a>
          </div>
          <Navbar.Collapse id='header-nav' style={{ alignSelf: 'flex-end' }}>
            <Nav className='ml-auto'>
              <Link href='/features' passHref><Nav.Link>Features</Nav.Link></Link>
              <Link href='/blog' passHref><Nav.Link>Blog</Nav.Link></Link>
              <Link href='/pricing-alpha' passHref><Nav.Link>Pricing</Nav.Link></Link>
              <Nav.Link href='https://app.getunify.co/login' className='d-md-none'>Login</Nav.Link>
            </Nav>
          </Navbar.Collapse>
          <div className='get-started d-none d-md-block'>
            <ButtonLink href='/pricing-alpha' type='primary'>Get Started</ButtonLink>
          </div>
        </Navbar>
      </Container>
      <style jsx>{`
          .top-right {
            position: absolute;
            top: 8px;
            right: 45px;
            padding: .5rem;
          }
          .get-started {
            position: relative;
            top: 30px;
            right: 0;
            margin-left: 30px;
          }
        `}</style>
    </div>
  )
}
export default Header
