import React from 'react'
import Link from 'next/link'
import { Container, Row, Col } from 'react-bootstrap'
import ContactUsForm from './Footer/ContactUsForm'
import SubscribeForm from './Footer/SubscribeForm'

const Footer = () => {
  return (
    <Container>
      <Row>
        <Col md>
          <ContactUsForm />
        </Col>
        <Col md={{ order: 12 }}>
          <SubscribeForm />
        </Col>
        <Col md={{ order: 1 }}>
          <ul className='links'>
            <li>
              <Link href='/features'>
                <a>Features</a>
              </Link>
            </li>
            <li>
              <Link href='/blog'>
                <a>Blog</a>
              </Link>
            </li>
            <li>
              <Link href='/about'>
                <a>About</a>
              </Link>
            </li>
            <li>
              <Link href='/terms'>
                <a>Terms and Conditions</a>
              </Link>
            </li>
          </ul>
          <div id='social_links'>
            <a href='#'>
              <img src='/images/instagram.png' alt='Instagram' />
            </a>
            <a href='#'>
              <img src='/images/linkedin.png' alt='Linkedin' />
            </a>
            <a href='#'>
              <img src='/images/facebook.png' alt='Facebook' />
            </a>
          </div>
        </Col>
      </Row>
      <style jsx>{`
        .links {
          text-align: center;
          list-style: none;
          padding: 0;
        }
        .links a {
          color: #fff;
        }
        #social_links {
          margin-top: 15px;
          text-align: center;
        }
        #social_links > a {
          display: inline-block;
          margin: 0 10px;
        }
        #social_links > a > img {
          width: 50px;
        }
      `}</style>
    </Container>
  )
}

export default Footer
