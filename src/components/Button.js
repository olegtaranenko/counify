import React from 'react'
import styled from 'styled-components'

const Btn = styled.a`
  border-radius: 20px;
  padding: 10px;
  color: #fff !important;
  apperance: none !important;
  -webkit-appearance: none !important;
  display: inline-block;
  transition: background-color 0.5s ease;
  :hover {
    background-color: #a840de;
  }
  background-color: ${props => {
    switch (props.type) {
      case 'primary':
        return props.theme.colors.primary
      case 'purple':
        return props.theme.colors.primary
      case 'gray':
        return props.theme.colors.gray
      default:
        return props.theme.colors.primary
    }
  }}
  :hover {
    background-color: #a840de;
  }
`

const Button = (props) => {
  return (
    <Btn className={props.className} type='button' onClick={props.onClick ? props.onClick : null}>{ props.children }</Btn>
  )
}

export default Button
