export default {
  env: process.env.NODE_ENV,
  mode: process.env.MODE,
  api_base_url: process.env.API_ENDPOINT,
  stripe_key: process.env.STRIPE_KEY
}
