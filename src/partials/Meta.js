import Head from 'next/head'
const Meta = (props) => (
  <Head>
    <title>{props.title}</title>
    <meta name='description' content={props.desc} />
    <meta property='og:type' content='website' />
    <meta name='og:title' property='og:title' content={props.title} />
    <meta name='og:description' property='og:description' content={props.desc} />
    <meta property='og:site_name' content='Unify' />
    <meta property='og:url' content={`${props.canonical}`} />
    <meta name='twitter:card' content='summary' />
    <meta name='twitter:title' content={props.title} />
    <meta name='twitter:description' content={props.desc} />
    <meta name='twitter:site' content='@getunify' />
    <meta name='twitter:creator' content='@getunify' />
    <link rel='icon' href='/images/favicon.ico' />
    <link rel='apple-touch-icon' href='/images/apple-touch-icon.png' />
    {
      props.image ? (
        <meta property='og:image' content={`${props.image}`} />
      ) : (
        <meta property='og:image' content='https://www.getunify.co/images/logo.png' />
      )
    }
    {props.canonical && <link rel='canonical' href={`${props.canonical}`} />}
    {props.image && <meta name='twitter:image' content={`${props.image}`} />}
    {props.noFollow && <meta name='robots' content='nofollow, noindex' />}
  </Head>
)
export default Meta
