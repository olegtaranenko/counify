import axios from 'axios'
import { toast } from 'react-toastify'
import { SUBMIT_CONTACT_FORM, SUBMIT_SUBSCRIBE_FORM, SUBMIT_REGISTER_FORM } from './types'

export const submitContactForm = (form) => dispatch => {
  return new Promise((resolve, reject) => {
    axios.post('contact-us', form)
      .then(response => response.data)
      .then(data => {
        dispatch({
          type: SUBMIT_CONTACT_FORM,
          payload: form
        })
        toast.success('Your Message Has Been Sent!')
        resolve(data)
      })
      .catch(err => {
        toast.error('An unknown error has occured!')
        reject(err.response.data)
      })
  })
}

export const submitSubscribeForm = (form) => dispatch => {
  return new Promise((resolve, reject) => {
    axios.post('newsletter', form)
      .then(response => response.data)
      .then(data => {
        dispatch({
          type: SUBMIT_SUBSCRIBE_FORM,
          payload: form
        })
        toast.success('Your have been subscribed!')
        resolve(data)
      })
      .catch(err => {
        toast.error('An unknown error has occured!')
        reject(err.response.data)
      })
  })
}

export const submitRegisterForm = (form) => dispatch => {
  return new Promise((resolve, reject) => {
    axios.post('register', form)
      .then(response => response.data)
      .then((data) => {
        dispatch({
          type: SUBMIT_REGISTER_FORM,
          payload: form
        })
        resolve(data)
      })
      .catch(err => {
        reject(err.response.data)
      })
  })
}
