import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Formik, Field } from 'formik'
import { Row, Col, Form } from 'react-bootstrap'
import { connect } from 'react-redux'
import Router from 'next/router'
import { CardElement } from '@stripe/react-stripe-js'
import * as Yup from 'yup'
import { submitRegisterForm } from './../actions/general'

const cardElementOptions = {
  iconStyle: 'solid',
  style: {
    base: {
      iconColor: '#b540de',
      fontFamily: '"PT Serif", Helvetica, Arial, serif',
      fontSize: '16px'
    }
  }
}

const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/

const registerSchema = Yup.object().shape({
  company_name: Yup.string()
    .required('Company name is required'),
  first_name: Yup.string()
    .required('First name is required'),
  last_name: Yup.string()
    .required('Last name is required'),
  email: Yup.string()
    .required('Email is required')
    .email('Enter a valid email address'),
  phone: Yup.string()
    .required('Phone number is required')
    .matches(phoneRegExp, 'Phone number is not valid'),
  password: Yup.string()
    .required('Password is required')
})

class RegisterContainer extends Component {
  constructor (props) {
    super(props)
    this.state = {
      serverErrors: null
    }
  }

  async onFormSubmit (data, { setSubmitting }) {
    setSubmitting(true)
    const alpha = this.props.alpha || false
    const form = { ...data, alpha }
    const { stripe, elements } = this.props
    if (stripe) {
      const { error, paymentMethod } = await stripe.createPaymentMethod({
        type: 'card',
        card: elements.getElement(CardElement)
      })
      if (error) {
        // TODO show some error
        return
      }
      form['payment_id'] = paymentMethod.id
    }
    this.props.submitRegisterForm(form).then(() => {
      Router.push('/register-success')
    })
      .catch(err => {
        if (err.errors) {
          this.setState({
            serverErrors: err.errors
          })
        }
      })
    setSubmitting(false)
  }

  render () {
    const { stripe } = this.props
    const { serverErrors } = this.state
    return (
      <div id='register-wrap'>
        <Row>
          <Col md={{ span: 6, offset: 3 }} className='text-center'>
            <img src='/images/register.png' alt='Register' className='register-person-image' />
            <div className='form-title'>
              <span>SIGN UP</span>
            </div>
            <Formik
              validationSchema={registerSchema}
              validateOnChange={false}
              validateOnBlur={false}
              initialValues={
                {
                  company_name: '',
                  first_name: '',
                  last_name: '',
                  email: '',
                  phone: '',
                  password: ''
                }
              }
              onSubmit={this.onFormSubmit.bind(this)}>
              {({ errors, isSubmitting, handleSubmit }) => (
                <form onSubmit={handleSubmit} className='form-default'>
                  <Form.Group controlId='company_name'>
                    <label>Company Name</label>
                    <Field name='company_name' as='input' />
                    {errors.company_name && <span className='field-error'>{errors.company_name}</span>}
                  </Form.Group>
                  <Form.Group controlId='first_name'>
                    <label>First Name</label>
                    <Field name='first_name' as='input' />
                    {errors.first_name && <span className='field-error'>{errors.first_name}</span>}
                  </Form.Group>
                  <Form.Group controlId='last_name'>
                    <label>Last Name</label>
                    <Field name='last_name' as='input' />
                    {errors.last_name && <span className='field-error'>{errors.last_name}</span>}
                  </Form.Group>
                  <Form.Group controlId='email'>
                    <label>Email</label>
                    <Field name='email' as='input' />
                    {errors.email && <span className='field-error'>{errors.email}</span>}
                    {(serverErrors && serverErrors.email) && <span className='field-error'>{serverErrors.email[0]}</span>}
                  </Form.Group>
                  <Form.Group controlId='phone'>
                    <label>Phone Number</label>
                    <Field name='phone' as='input' />
                    {errors.phone && <span className='field-error'>{errors.phone}</span>}
                  </Form.Group>
                  <Form.Group controlId='password'>
                    <label>Password</label>
                    <Field name='password' type='password' />
                    {errors.password && <span className='field-error'>{errors.password}</span>}
                  </Form.Group>
                  {stripe &&
                    <Form.Group>
                      <label>Payment</label>
                      <div id='card-element-wrap'>
                        <CardElement options={cardElementOptions} />
                      </div>
                    </Form.Group>
                  }
                  <button className='btn btn-purple squared' disabled={isSubmitting} type='submit'>Sign Up</button>
                </form>
              )}
            </Formik>
          </Col>
        </Row>
        <style jsx>{`
          #register-wrap {
            font-family: "PT Serif", Helvetica, Arial, serif;
            margin: 10px 0;
          }
          .register-person-image {
            width: 200px;
          }
          button {
            width: 100%;
          }
          #card-element-wrap {
            padding: .375rem 0;
          }
        `}</style>
      </div>
    )
  }
};

RegisterContainer.propTypes = {
  alpha: PropTypes.bool
}

export default connect(null, {
  submitRegisterForm
})(RegisterContainer)
