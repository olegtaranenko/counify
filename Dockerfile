FROM node:12

WORKDIR /app

COPY ./ /app/
RUN yarn install && npm run build

EXPOSE 3100

CMD [ "yarn", "serve" ]