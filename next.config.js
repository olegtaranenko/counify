const { ANALYZE, ASSET_HOST } = process.env
const withSass = require('@zeit/next-sass')
const withCSS = require('@zeit/next-css')

// for those who using CDN
const assetPrefix = ASSET_HOST || ''

module.exports = withCSS(withSass({
  assetPrefix,
  target: 'serverless',
  webpack: (config, { dev }) => {
    config.output.publicPath = `${assetPrefix}${config.output.publicPath}`

    if (ANALYZE) {
      const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer')
      config.plugins.push(new BundleAnalyzerPlugin({
        analyzerMode: 'server',
        analyzerPort: 8888,
        openAnalyzer: true
      }))
    }

    return config
  }
}))
